function values(obj) {
    const arr = [];
    for (let property in obj ){
      if(typeof obj[property] === "function") {
          return null;
      }
      else{
          arr.push(obj[property]);
      }
    }
    return arr;
  }
  module.exports = values;