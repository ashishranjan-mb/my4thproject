function mapObject(obj, cb) {
    const maping = {}
    for (let property in obj){
      maping[property] = cb(obj[property]);
    }
    return maping;
}
module.exports = mapObject;
