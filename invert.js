function invert(obj) {
    const newobject = {};
    for(let key in obj) {
        newobject[obj[key]] = key;
    }
    return newobject;
}
module.exports = invert;
